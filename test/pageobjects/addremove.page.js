const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */

class AddRemovePage extends Page{
      /**
     * define selectors using getter methods
     */

    get pageTitle(){
        return $('h3');
    }
    
      
    get addElementButton(){
        return $('#content button');
    }

    deteledButtons(index){
        { return $(`#elements .added-manually:nth-child(${index})`); }
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. adding or removing buttons
     */


     async addElement(){
        for(let i=0; i<4; i++){
            await this.addElementButton.click();
        }        
    }

    async deleteElement(index){
        await this.deteledButtons(index).click();
    }
    
    /**
     * overwrite specific options to adapt it to page object
     */

    open(){
        return super.open('add_remove_elements/');
    }   

}

module.exports = new AddRemovePage();
