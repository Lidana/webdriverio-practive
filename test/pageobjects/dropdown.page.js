const Page = require('./page');

/**
 * sub page containing the selectors and methods for the dropdwon pages
 */

class Dropdown extends Page{
    /**
     * Define the selectos using getter methods
     */

    get pageTitle(){
        return $('h3');
    }

    get dropdown(){
        return $('.example #dropdown');
    }

    dropdownPositions(index){
        {return $(`#dropdown option:nth-child(${index})`)}
    }


    /**
     * Methods to interact with the page
     */

    async clickOnDropdwon(){
        await this.dropdown.click();
        console.log('test1')
    }

    async clickOnDropdwonPosition(index){
        //await this.dropdown(index).click();
    }    

    /**
     * overwrite open method to get dropdown options
     */
    open(){
        return super.open('dropdown');
    }


}

module.exports = new Dropdown();