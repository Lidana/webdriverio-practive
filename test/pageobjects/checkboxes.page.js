const Page = require('./page');

/**
 * sub page containing the selector for the forgot Password page
 */

class CheckBoxes extends Page{
    /**
     * Define selector using getter methods
     */

    get pageTitle(){
        return $('h3');
    }

    checkBoxesPosition(index){
        {return $(`#checkboxes input:nth-child(${index})`)}
    }

    /**
     * methods to validate actions over the page
     */

    async clickOnCheckBox(index){
        await this.checkBoxesPosition(index).click();
    }

    /**
     * overwrite open method to get checkboxes url Page
     */

    open(){
        return super.open('checkboxes');
    }

    
}

module.exports = new CheckBoxes();