const AddRemovePage = require('../pageobjects/addremove.page');


describe('Should add element', ()=>{
    it('Should add a new button', async() =>{
        await AddRemovePage.open();
        await expect(AddRemovePage.pageTitle).toHaveTextContaining('Add/Remove Elements');
        await AddRemovePage.addElement();
        await expect(AddRemovePage.addElementButton).toBeDisplayed();
        await AddRemovePage.deleteElement(1);
    });
});