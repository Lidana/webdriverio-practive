const CheckBoxes = require('../pageobjects/checkboxes.page')

describe('Checkbox Page Validation', () => {
    it('Should select a checkbox', async() =>{
        await CheckBoxes.open();
        await expect(CheckBoxes.pageTitle).toHaveTextContaining(
            'Checkboxes'
        );
        await CheckBoxes.clickOnCheckBox(1);
        await expect(CheckBoxes.checkBoxesPosition(1)).toBeChecked();
    })
})
