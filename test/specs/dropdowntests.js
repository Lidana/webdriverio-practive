const Dropdown = require('../pageobjects/dropdown.page');

describe('Dropdwon Page Validation', () => {
    it('Should select a dropdown option from the list', async() => {
        await Dropdown.open();

        await expect(Dropdown.pageTitle).toHaveTextContaining('Dropdown List');
        await Dropdown.clickOnDropdwon();
        await Dropdown.clickOnDropdwonPosition(2);
        await expect(Dropdown.dropdownPositions(3)).toBeEnabled();

    })
})

